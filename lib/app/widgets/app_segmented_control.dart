import 'package:flutter/cupertino.dart';
import 'package:material_segmented_control/material_segmented_control.dart';
import 'package:tomisha_test/l10n/l10n.dart';

class AppSegmentedControl extends StatefulWidget {
  const AppSegmentedControl({
    required this.onChanged,
    super.key,
  });

  final void Function(int) onChanged;

  @override
  State<AppSegmentedControl> createState() => _AppSegmentedControlState();
}

class _AppSegmentedControlState extends State<AppSegmentedControl> {
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    final l10n = AppLocalizations.of(context);
    return MaterialSegmentedControl(
      selectionIndex: selectedIndex,
      onSegmentTapped: (value) {
        setState(() {
          selectedIndex = value;
        });

        widget.onChanged(value);
      },
      borderColor: const Color(0xFFCBD5E0),
      unselectedColor: const Color(0xFFFFFFFF),
      selectedColor: const Color(0xFF81E6D9),
      selectedTextStyle: const TextStyle(color: Color(0xFFE6FFFA)),
      unselectedTextStyle: const TextStyle(color: Color(0xFF319795)),
      borderRadius: 12,
      children: {
        0: SizedBox(width: 160, child: Center(child: Text(l10n.arbeitnehmer))),
        1: SizedBox(width: 160, child: Center(child: Text(l10n.arbeitgeber))),
        2: SizedBox(width: 160, child: Center(child: Text(l10n.temporarburo))),
      },
    );
  }
}
