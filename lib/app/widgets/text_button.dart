import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class AppTextButton extends StatefulWidget {
  const AppTextButton({super.key, required this.text});
  final String text;

  @override
  State<AppTextButton> createState() => _AppTextButtonState();
}

class _AppTextButtonState extends State<AppTextButton> {
  bool isHovering = false;
  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      child: Text(
        widget.text,
        style: TextStyle(
            decoration:
                isHovering ? TextDecoration.underline : TextDecoration.none),
      ),
      onEnter: (_) => setState(() {
        isHovering = true;
      }),
      onExit: (_) => setState(() {
        isHovering = false;
      }),
    );
  }
}
