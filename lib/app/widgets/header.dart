import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:tomisha_test/app/widgets/app_buttons.dart';
import 'package:tomisha_test/l10n/l10n.dart';
import 'package:tomisha_test/res/assets.dart';
import 'package:tomisha_test/res/text_styles.dart';

class Header extends StatelessWidget {
  const Header({super.key});

  @override
  Widget build(BuildContext context) {
    final l10n = AppLocalizations.of(context);
    return SizedBox(
      width: double.infinity,
      height: 660,
      child: SquigglyBackground(
        child: ScreenTypeLayout.builder(
          mobile: (_) {
            return SizedBox(
              height: 660,
              width: double.infinity,
              child: Column(
                children: [
                  Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 100, vertical: 18),
                      child: Text(
                        l10n.deineJobWebsite,
                        style: AppTextStyle.header,
                        textAlign: TextAlign.center,
                      )),
                  SizedBox(
                    height: 470,
                    child: SvgPicture.asset(
                      AssetFiles.agreement,
                      width: double.infinity,
                      fit: BoxFit.fitHeight,
                    ),
                  )
                ],
              ),
            );
          },
          desktop: (_) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: 320,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        l10n.deineJobWebsite,
                        style: AppTextStyle.desktopHeader,
                      ),
                      const SizedBox(
                        height: 53,
                      ),
                      AppGradientButton(
                        onPressed: () {},
                        text: l10n.registerForFree,
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  width: 151,
                ),
                CircularContainer(
                  radius: 227.5,
                  child: SvgPicture.asset(
                    AssetFiles.agreement,
                    width: 460,
                    fit: BoxFit.fill,
                  ),
                ),
                const SizedBox(
                  width: 151,
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}

class CircularContainer extends StatelessWidget {
  final double radius;
  final Widget child;

  CircularContainer({required this.radius, required this.child});

  @override
  Widget build(BuildContext context) {
    return ClipOval(
      child: Container(
        width: radius * 2,
        height: radius * 2,
        color: Colors.white, // Change the color as needed
        child: Center(
          child: child,
        ),
      ),
    );
  }
}

class SquigglyBackground extends StatelessWidget {
  const SquigglyBackground({required this.child, super.key});
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        CustomPaint(
          painter: CurvedBottomPainter(),
          child: child,
        ),
      ],
    );
  }
}

class CurvedBottomPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final colors = [const Color(0xFFEBF4FF), const Color(0xFFE6FFFA)];
    final stops = [0.0, 1.0];
    final gradient = ui.Gradient.linear(
      Offset.zero,
      Offset(size.width, size.height / 2),
      colors,
      stops,
    );
    final paint = Paint()..shader = gradient;

    final rect = Rect.fromLTWH(0, 0, size.width, size.height - 100);

    final curveOriginHeight = size.height - 100;

    final path = Path()
      ..moveTo(0, curveOriginHeight)
      ..lineTo(0, curveOriginHeight)
      ..lineTo(0, size.height)
      ..cubicTo(
        size.width / 4,
        size.height,
        3 * size.width / 4,
        curveOriginHeight,
        size.width,
        curveOriginHeight,
      );

    canvas
      ..drawRect(rect, paint)
      ..drawPath(path, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}
