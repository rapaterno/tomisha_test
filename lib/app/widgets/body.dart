import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:tomisha_test/app/widgets/sections/section_one.dart';
import 'package:tomisha_test/app/widgets/sections/section_three.dart';
import 'package:tomisha_test/app/widgets/sections/section_two.dart';
import 'package:tomisha_test/l10n/l10n.dart';
import 'package:tomisha_test/res/assets.dart';
import 'package:tomisha_test/res/text_styles.dart';

enum Segment { arbeitnerhmer, arbeitgeber, temporarburo }

class Body extends StatelessWidget {
  const Body({super.key, required this.selection});

  final Segment selection;

  @override
  Widget build(BuildContext context) {
    final l10n = AppLocalizations.of(context);

    final headerText = switch (selection) {
      Segment.arbeitnerhmer => l10n.arbeitnehmerHeader,
      Segment.arbeitgeber => l10n.arbeitgeberHeader,
      Segment.temporarburo => l10n.temporarburoHeader,
    };
    final sectionOneText = switch (selection) {
      Segment.arbeitnerhmer => l10n.erstellenDeinLebenslauf,
      Segment.arbeitgeber => l10n.erstellenDeinLebenslauf,
      Segment.temporarburo => l10n.erstellenDeinUnternehmensprofil,
    };
    final sectionTwoText = switch (selection) {
      Segment.arbeitnerhmer => l10n.erstellenDeinLebenslauf,
      Segment.arbeitgeber => l10n.erstellenEinJobinserat,
      Segment.temporarburo => l10n.erhalteVermittlungs,
    };
    final sectionThreeText = switch (selection) {
      Segment.arbeitnerhmer => l10n.mitNurEinemKlick,
      Segment.arbeitgeber => l10n.wahleDeinenNeuen,
      Segment.temporarburo => l10n.vemittlungNach,
    };

    return ResponsiveBuilder(
      builder: (BuildContext context, SizingInformation sizingInformation) {
        final isDesktop =
            sizingInformation.deviceScreenType == DeviceScreenType.desktop;
        return Stack(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const SizedBox(height: 30),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 50),
                  child: Center(
                    child: Text(
                      headerText,
                      maxLines: 2,
                      style: AppTextStyle.subheader,
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                SizedBox(
                  width: double.infinity,
                  height: 254,
                  child: Center(child: SectionOne(sectionOneText)),
                ),
                if (isDesktop)
                  const SizedBox(
                    height: 190,
                  ),
                SizedBox(
                  width: double.infinity,
                  height: 370,
                  child: SectionTwo(sectionTwoText),
                ),
                if (isDesktop)
                  const SizedBox(
                    height: 20,
                  ),
                SizedBox(
                  width: double.infinity,
                  height: 370,
                  child: Center(child: SectionThree(sectionThreeText)),
                ),
                const SizedBox(
                  height: 100,
                ),
              ],
            ),
            if (isDesktop)
              Positioned(
                top: 280,
                left: (MediaQuery.of(context).size.width - 850) / 2,
                child: SvgPicture.asset(AssetFiles.arrowOne),
              ),
            if (isDesktop)
              Positioned(
                top: 787,
                right: (MediaQuery.of(context).size.width - 550) / 2,
                child: SvgPicture.asset(AssetFiles.arrowTwo),
              ),
          ],
        );
      },
    );
  }
}
