import 'dart:ui' as ui;
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:tomisha_test/l10n/l10n.dart';
import 'package:tomisha_test/res/assets.dart';
import 'package:tomisha_test/res/text_styles.dart';

class SectionTwo extends StatelessWidget {
  const SectionTwo(this.text, {super.key});

  final String text;
  @override
  Widget build(BuildContext context) {
    return BodySection(
      background: const SquigglySectionBackground(),
      child: ScreenTypeLayout.builder(
        mobile: (_) => _MobileView(text: text),
        desktop: (_) => _DesktopView(text: text),
      ),
    );
  }
}

class _DesktopView extends StatelessWidget {
  const _DesktopView({super.key, required this.text});
  final String text;
  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: const BoxConstraints(maxWidth: 1200),
      child: Row(
        children: [
          const Expanded(
            flex: 3,
            child: SizedBox.shrink(),
          ),
          // ConstrainedBox(constraints: BoxConstraints.expand(width: 300)),
          SvgPicture.asset(
            AssetFiles.task,
            height: 227,
            width: 324,
          ),
          const SizedBox(
            width: 122.5,
          ),
          Expanded(flex: 4, child: _Title(text)),
        ],
      ),
    );
  }
}

class _MobileView extends StatelessWidget {
  const _MobileView({super.key, required this.text});
  final String text;
  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: const BoxConstraints(maxWidth: 320),
      child: Stack(
        fit: StackFit.expand,
        children: [
          Positioned(
            bottom: 60,
            right: 20,
            child: SvgPicture.asset(
              AssetFiles.task,
              width: 181,
              height: 127,
            ),
          ),
          Positioned(
            left: 20,
            top: 20,
            right: 0,
            child: _Title(text),
          ),
        ],
      ),
    );
  }
}

class _Title extends StatelessWidget {
  const _Title(this.text, {super.key});
  final String text;
  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (context, sizingInformation) {
        return Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Text(
              '2.',
              style: AppTextStyle.numberedBullet,
            ),
            const SizedBox(
              width: 23,
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(bottom: 25),
                child: Text(
                  text,
                  style: sizingInformation.deviceScreenType ==
                          DeviceScreenType.desktop
                      ? AppTextStyle.titleDesktop
                      : AppTextStyle.title,
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}

class BodySection extends StatelessWidget {
  const BodySection({
    required this.child,
    super.key,
    this.background,
  });

  final Widget? background;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 370,
      width: double.infinity,
      child: Stack(
        children: [
          if (background != null)
            const Positioned.fill(
              child: SquigglySectionBackground(),
            ),
          Positioned.fill(
            child: Container(
              constraints: const BoxConstraints(maxWidth: 320),
              child: Center(child: child),
            ),
          ),
        ],
      ),
    );
  }
}

class CurvedDesktopBackground extends CustomPainter {
  @override
  void paint(ui.Canvas canvas, ui.Size size) {
    final path_0 = Path()
      ..moveTo(size.width * 0.8801254, size.height * 0.02793407)
      ..cubicTo(
        size.width * 0.7591799,
        size.height * -0.04800080,
        size.width * 0.4994115,
        size.height * 0.09147040,
        size.width * 0.4662615,
        size.height * 0.09147040,
      )
      ..cubicTo(
        size.width * 0.4331114,
        size.height * 0.09147040,
        size.width * 0.3684708,
        size.height * 0.07862371,
        size.width * 0.2192861,
        size.height * 0.02793407,
      )
      ..cubicTo(
        size.width * 0.08924264,
        size.height * -0.01623263,
        size.width * 0.01652677,
        size.height * 0.01663098,
        0,
        size.height * 0.02579296,
      )
      ..lineTo(0, size.height * 0.9967634)
      ..cubicTo(
        size.width * 0.03671973,
        size.height * 1.014440,
        size.width * 0.2694260,
        size.height * 0.9547876,
        size.width * 0.3552050,
        size.height * 0.9384056,
      )
      ..cubicTo(
        size.width * 0.4447178,
        size.height * 0.9212767,
        size.width * 0.5325615,
        size.height * 0.9512523,
        size.width * 0.6386493,
        size.height * 0.9384056,
      )
      ..cubicTo(
        size.width * 0.7435601,
        size.height * 0.9257083,
        size.width * 0.9489822,
        size.height * 0.8293582,
        size.width,
        size.height * 0.6948165,
      )
      ..lineTo(size.width, size.height * 0.2266096)
      ..cubicTo(
        size.width * 0.9719344,
        size.height * 0.1229398,
        size.width * 0.9113748,
        size.height * 0.04760245,
        size.width * 0.8801254,
        size.height * 0.02793407,
      )
      ..close();

    final colors = [const Color(0xFFE6FFFA), const Color(0xFFEBF4FF)];
    final stops = [0.0, 1.0];
    final gradient = ui.Gradient.linear(
      Offset.zero,
      Offset(size.width, size.height / 2),
      colors,
      stops,
    );
    final paint = Paint()..shader = gradient;

    canvas.drawPath(path_0, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}

class SquigglySectionBackground extends StatelessWidget {
  const SquigglySectionBackground({super.key});

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (context, sizingInformation) {
        return CustomPaint(
          painter:
              sizingInformation.deviceScreenType == DeviceScreenType.desktop
                  ? CurvedDesktopBackground()
                  : CurvedBottomPainter(),
        );
      },
    );
  }
}

class CurvedBottomPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final path_0 = Path()
      ..moveTo(size.width * 0.009967320, size.height * 0.01208978)
      ..cubicTo(
        0,
        size.height * 0.01208978,
        size.width * 0.05098039,
        size.height * 0.005578641,
        size.width * 0.1618627,
        size.height * 0.03253934,
      )
      ..cubicTo(
        size.width * 0.2283824,
        size.height * 0.04871904,
        size.width * 0.3317810,
        size.height * 0.1025096,
        size.width * 0.4337418,
        size.height * 0.1053889,
      )
      ..cubicTo(
        size.width * 0.6200327,
        size.height * 0.1106403,
        size.width * 0.8102778,
        size.height * -0.01091189,
        size.width,
        size.height * 0.03765992,
      )
      ..lineTo(size.width, size.height * 0.8798384)
      ..cubicTo(
        size.width,
        size.height * 0.8798384,
        size.width * 0.7643301,
        size.height,
        size.width * 0.6252124,
        size.height,
      )
      ..cubicTo(
        size.width * 0.4860948,
        size.height,
        size.width * 0.4375817,
        size.height * 0.8913392,
        size.width * 0.2652614,
        size.height * 0.8913392,
      )
      ..cubicTo(
        size.width * 0.09294118,
        size.height * 0.8913392,
        0,
        size.height,
        0,
        size.height,
      )
      ..lineTo(0, size.height * 0.01208978)
      ..close();

    final colors = [const Color(0xFFE6FFFA), const Color(0xFFEBF4FF)];
    final stops = [0.0, 1.0];
    final gradient = ui.Gradient.linear(
      Offset.zero,
      Offset(size.width, size.height / 2),
      colors,
      stops,
    );
    final paint = Paint()..shader = gradient;

    canvas.drawPath(path_0, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}
