import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:tomisha_test/l10n/l10n.dart';
import 'package:tomisha_test/res/assets.dart';
import 'package:tomisha_test/res/text_styles.dart';

class SectionThree extends StatelessWidget {
  const SectionThree(this.text, {super.key});

  final String text;

  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout.builder(
      mobile: (_) => _MobileView(text: text),
      desktop: (_) => _DesktopView(text: text),
    );
  }
}

class _DesktopView extends StatelessWidget {
  const _DesktopView({super.key, required this.text});
  final String text;
  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: const BoxConstraints(maxWidth: 1200),
      child: Stack(
        children: [
          Positioned(
            top: 50,
            left: 200,
            child: Container(
              width: 304,
              height: 304,
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
                color: Color(0xFFF7FAFC),
              ),
            ),
          ),
          Row(
            children: [
              const Expanded(child: SizedBox.shrink()),
              SizedBox(width: 400, child: _Title(text)),
              SvgPicture.asset(
                AssetFiles.personalFile,
                height: 376,
                width: 502,
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class _MobileView extends StatelessWidget {
  const _MobileView({super.key, required this.text});
  final String text;

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: const BoxConstraints(maxWidth: 320),
      child: Stack(
        fit: StackFit.expand,
        children: [
          Positioned(
            bottom: 0,
            right: 20,
            child: SvgPicture.asset(
              AssetFiles.personalFile,
              height: 210,
              width: 281,
            ),
          ),
          Positioned(
            left: 40,
            right: 0,
            top: 20,
            child: _Title(text),
          ),
        ],
      ),
    );
  }
}

class _Title extends StatelessWidget {
  const _Title(this.text, {super.key});

  final String text;

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (context, sizingInformation) {
        return Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Text(
              '3.',
              style: AppTextStyle.numberedBullet,
            ),
            const SizedBox(
              width: 23,
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(bottom: 25),
                child: Text(
                  text,
                  style: sizingInformation.deviceScreenType ==
                          DeviceScreenType.desktop
                      ? AppTextStyle.titleDesktop
                      : AppTextStyle.title,
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}
