import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:tomisha_test/l10n/l10n.dart';
import 'package:tomisha_test/res/assets.dart';
import 'package:tomisha_test/res/text_styles.dart';

class SectionOne extends StatelessWidget {
  const SectionOne(this.text, {super.key});

  final String text;

  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout.builder(
      mobile: (_) => _MobileView(text: text),
      desktop: (_) => _DesktopView(text: text),
    );
  }
}

class _Title extends StatelessWidget {
  const _Title(this.text, {super.key});
  final String text;

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (context, sizingInformation) {
        return Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Text(
              '1.',
              style: AppTextStyle.numberedBullet,
            ),
            const SizedBox(
              width: 23,
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 25),
              child: Text(
                text,
                style: sizingInformation.deviceScreenType ==
                        DeviceScreenType.desktop
                    ? AppTextStyle.titleDesktop
                    : AppTextStyle.title,
              ),
            ),
          ],
        );
      },
    );
  }
}

class _DesktopView extends StatelessWidget {
  const _DesktopView({super.key, required this.text});

  final String text;

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: const BoxConstraints(maxWidth: 1050),
      child: Row(
        children: [
          _Title(text),
          const SizedBox(
            width: 61,
          ),
          SvgPicture.asset(
            AssetFiles.profileData,
            height: 253,
            width: 384,
          ),
        ],
      ),
    );
  }
}

class _MobileView extends StatelessWidget {
  const _MobileView({super.key, required this.text});
  final String text;
  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: const BoxConstraints(maxWidth: 320),
      child: Stack(
        fit: StackFit.expand,
        children: [
          Positioned(
            top: 6,
            right: 20,
            child: SvgPicture.asset(
              AssetFiles.profileData,
              height: 145,
              width: 220,
            ),
          ),
          Positioned(
            left: 0,
            bottom: 0,
            child: _Title(text),
          ),
        ],
      ),
    );
  }
}
