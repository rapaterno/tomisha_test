import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:tomisha_test/app/widgets/app_segmented_control.dart';
import 'package:tomisha_test/app/widgets/body.dart';
import 'package:tomisha_test/app/widgets/header.dart';
import 'package:tomisha_test/app/widgets/text_button.dart';
import 'package:tomisha_test/l10n/l10n.dart';

import '../widgets/app_buttons.dart';

class IndexPage extends StatefulWidget {
  const IndexPage({super.key});

  @override
  State<IndexPage> createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage> {
  Segment selection = Segment.arbeitnerhmer;
  bool isTopButtonHidden = true;
  final scrollController = ScrollController();

  @override
  void initState() {
    scrollController.addListener(_handleControllerNotification);
    super.initState();
  }

  @override
  void dispose() {
    scrollController.removeListener(_handleControllerNotification);
    super.dispose();
  }

  void _handleControllerNotification() {
    if (scrollController.offset > 464 && isTopButtonHidden) {
      setState(() {
        isTopButtonHidden = false;
      });
    } else if (scrollController.offset < 464 && !isTopButtonHidden) {
      setState(() {
        isTopButtonHidden = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final l10n = AppLocalizations.of(context);

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(67),
        child: TopNavBar(isTopButtonHidden: isTopButtonHidden),
      ),
      bottomNavigationBar: ScreenTypeLayout.builder(
        mobile: (_) {
          return Container(
            height: 80,
            width: double.infinity,
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(12),
                topRight: Radius.circular(12),
              ),
              boxShadow: [
                BoxShadow(
                  color: Color.fromARGB(15, 0, 0, 16),
                  offset: Offset(0, -3),
                  blurRadius: 6,
                )
              ],
            ),
            child: Center(
              child: AppGradientButton(
                text: l10n.registerForFree,
                onPressed: () {},
              ),
            ),
          );
        },
        desktop: (_) => const SizedBox.shrink(),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          controller: scrollController,
          child: Column(
            children: [
              const Header(),
              AppSegmentedControl(
                onChanged: (value) {
                  setState(() {
                    if (value == 0) {
                      selection = Segment.arbeitnerhmer;
                    } else if (value == 1) {
                      selection = Segment.arbeitgeber;
                    } else if (value == 2) {
                      selection = Segment.temporarburo;
                    }
                  });
                },
              ),
              Body(
                selection: selection,
              )
            ],
          ),
        ),
      ),
    );
  }
}

class TopNavBar extends StatelessWidget {
  const TopNavBar({super.key, required this.isTopButtonHidden});

  final bool isTopButtonHidden;

  @override
  Widget build(BuildContext context) {
    final l10n = AppLocalizations.of(context);
    return Container(
      height: 67,
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(12),
          bottomRight: Radius.circular(12),
        ),
        boxShadow: [
          BoxShadow(
            color: Color.fromARGB(15, 0, 0, 16),
            offset: Offset(0, 3),
            blurRadius: 6,
          )
        ],
      ),
      child: Column(
        children: [
          Container(
            height: 5,
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                colors: [Color(0xFF319795), Color(0xFF3182CE)],
              ),
            ),
          ),
          Expanded(
            child: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  ScreenTypeLayout.builder(
                    mobile: (p0) => const SizedBox.shrink(),
                    desktop: (p0) => isTopButtonHidden
                        ? const SizedBox.shrink()
                        : Row(
                            children: [
                              Text(l10n.jetzt),
                              const SizedBox(
                                width: 20,
                              ),
                              AppButton(
                                text: l10n.registerForFree,
                              ),
                            ],
                          ),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  AppTextButton(text: l10n.login),
                  const SizedBox(
                    width: 19,
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
