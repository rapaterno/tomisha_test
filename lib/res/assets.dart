abstract class AssetFiles {
  static const String assets = 'svgs/';

  static const String agreement = '${assets}undraw_agreement_aajr.svg';
  static const String task = '${assets}undraw_task_31wc.svg';
  static const String personalFile = '${assets}undraw_personal_file_222m.svg';
  static const String jobOffers = '${assets}undraw_job_offers_kw5d.svg';
  static const String businessDeal = '${assets}undraw_business_deal_cpi9.svg';
  static const String aboutMe = '${assets}undraw_about_me_wa29.svg';
  static const String swipeProfiles =
      '${assets}undraw_swipe_profiles1_i6mr.svg';
  static const String profileData = '${assets}undraw_Profile_data_re_v81r.svg';
  static const String arrowOne = '${assets}Gruppe_1821.svg';
  static const String arrowTwo = '${assets}Gruppe_1822.svg';
}
