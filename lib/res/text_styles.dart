import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';

abstract class AppTextStyle {
  static TextStyle header = GoogleFonts.lato(
    color: const Color(0xFF2D3748),
    fontWeight: FontWeight.w500,
    fontSize: 42,
  );
  static TextStyle desktopHeader = GoogleFonts.lato(
    color: const Color(0xFF2D3748),
    fontWeight: FontWeight.bold,
    fontSize: 65,
  );
  static TextStyle subheader = GoogleFonts.lato(
    color: const Color(0xFF4A5568),
    fontWeight: FontWeight.w500,
    fontSize: 21,
  );

  static TextStyle numberedBullet = GoogleFonts.lato(
    color: const Color(0xFF718096),
    fontSize: 130,
  );

  static TextStyle title = GoogleFonts.lato(
    color: const Color(0xFF718096),
    fontWeight: FontWeight.w500,
    fontSize: 16,
  );
  static TextStyle titleDesktop = GoogleFonts.lato(
    color: const Color(0xFF718096),
    fontWeight: FontWeight.w500,
    fontSize: 30,
  );
}
