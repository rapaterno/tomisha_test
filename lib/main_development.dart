import 'package:tomisha_test/app/app.dart';
import 'package:tomisha_test/bootstrap.dart';

void main() {
  bootstrap(() => const App());
}
